//Dunno how. ������ ����������� ��� � storage, ��������� ��� ���� �����. �� ���������, �� � ��� ���� ������� ���� verb, ��� �� ��������� � ��������� �����.

/obj/item/clothing/suit/storage
	name = "storage armor"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon_state = "storage_armor"
	desc = "Armor with little pockets for ammo."
	armor = list(melee = 25, bullet = 15, laser = 25, energy = 10, bomb = 25, bio = 0, rad = 0)
	w_class = 3
	var/silent = 0
	var/list/can_hold = new/list()
	var/list/cant_hold = new/list()
	var/list/is_seeing = new/list()
	var/max_w_class = 2
	var/max_combined_w_class = 14
	var/storage_slots = 3
	var/obj/screen/storage/boxes = null
	var/obj/screen/close/closer = null
	var/use_to_pickup
	var/display_contents_with_number
	var/collection_mode = 1
	var/preposition = "in"

/obj/item/clothing/suit/storage/proc/return_inv()
	var/list/L = list()
	L += contents

	for(var/obj/item/weapon/storage/S in src)
		L += S.return_inv()
	return L


/obj/item/clothing/suit/storage/proc/show_to(mob/user)
	if(user.s_active != src && (user.stat == CONSCIOUS))
		for(var/obj/item/I in src)
			if(I.on_found(user))
				return
	if(user.s_active)
		user.s_active.hide_from(user)
	if(user.client)
		user.client.screen -= boxes
		user.client.screen -= contents
		user.client.screen += boxes
		user.client.screen += contents
	user.s_active = src
	is_seeing |= user


/obj/item/clothing/suit/storage/throw_at(atom/target, range, speed, mob/thrower, spin)
	close_all()
	return ..()


/obj/item/clothing/suit/storage/proc/hide_from(mob/user)
	if(!user.client)
		return
	user.client.screen -= boxes
	user.client.screen -= contents
	if(user.s_active == src)
		user.s_active = null
	is_seeing -= user


/obj/item/clothing/suit/storage/proc/can_see_contents()
	var/list/cansee = list()
	for(var/mob/M in is_seeing)
		if(M.s_active == src && M.client)
			cansee |= M
		else
			is_seeing -= M
	return cansee


/obj/item/clothing/suit/storage/proc/close(mob/user)
	hide_from(user)
	user.s_active = null


/obj/item/clothing/suit/storage/verb/close_all()
	set name = "Close pockets"
	for(var/mob/M in can_see_contents())
		close(M)
		. = 1


/obj/item/clothing/suit/storage/proc/orient_objs(tx, ty, mx, my)
	var/cx = tx
	var/cy = ty
	boxes.screen_loc = "[tx]:,[ty] to [mx],[my]"
	for(var/obj/O in contents)
		O.screen_loc = "[cx],[cy]"
		O.layer = 20
		cx++
		if(cx > mx)
			cx = tx
			cy--
	closer.screen_loc = "[mx+1],[my]"


/obj/item/clothing/suit/storage/proc/standard_orient_objs(rows, cols, list/obj/item/display_contents)
	var/cx = 4
	var/cy = 2+rows
	boxes.screen_loc = "4:16,2:16 to [4+cols]:16,[2+rows]:16"

	if(display_contents_with_number)
		for(var/datum/numbered_display/ND in display_contents)
			ND.sample_object.mouse_opacity = 2
			ND.sample_object.screen_loc = "[cx]:16,[cy]:16"
			ND.sample_object.maptext = "<font color='white'>[(ND.number > 1)? "[ND.number]" : ""]</font>"
			ND.sample_object.layer = 20
			cx++
			if(cx > (4+cols))
				cx = 4
				cy--
	else
		for(var/obj/O in contents)
			O.mouse_opacity = 2
			O.screen_loc = "[cx]:16,[cy]:16"
			O.maptext = ""
			O.layer = 20
			cx++
			if(cx > (4+cols))
				cx = 4
				cy--
	closer.screen_loc = "[4+cols+1]:16,2:16"


/obj/item/clothing/suit/storage/proc/orient2hud(mob/user)
	var/adjusted_contents = contents.len

	var/list/datum/numbered_display/numbered_contents
	if(display_contents_with_number)
		numbered_contents = list()
		adjusted_contents = 0
		for(var/obj/item/I in contents)
			var/found = 0
			for(var/datum/numbered_display/ND in numbered_contents)
				if(ND.sample_object.type == I.type)
					ND.number++
					found = 1
					break
			if(!found)
				adjusted_contents++
				numbered_contents.Add( new/datum/numbered_display(I) )

	var/row_num = 0
	var/col_count = min(7,storage_slots) -1
	if(adjusted_contents > 7)
		row_num = round((adjusted_contents-1) / 7)
	standard_orient_objs(row_num, col_count, numbered_contents)

/obj/item/clothing/suit/storage/proc/can_be_inserted(obj/item/W, stop_messages = 0, mob/user)
	if(!istype(W) || (W.flags & ABSTRACT)) return

	if(loc == W)
		return 0
	if(contents.len >= storage_slots)
		if(!stop_messages)
			usr << "<span class='warning'>[src] is full, make some space!</span>"
		return 0

	if(can_hold.len)
		var/ok = 0
		for(var/A in can_hold)
			if(istype(W, A))
				ok = 1
				break
		if(!ok)
			if(!stop_messages)
				usr << "<span class='warning'>[src] cannot hold [W]!</span>"
			return 0

	for(var/A in cant_hold)
		if(istype(W, A))
			if(!stop_messages)
				usr << "<span class='warning'>[src] cannot hold [W]!</span>"
			return 0

	if(W.w_class > max_w_class)
		if(!stop_messages)
			usr << "<span class='warning'>[W] is too big for [src]!</span>"
		return 0

	var/sum_w_class = W.w_class
	for(var/obj/item/I in contents)
		sum_w_class += I.w_class

	if(sum_w_class > max_combined_w_class)
		if(!stop_messages)
			usr << "<span class='warning'>[W] won't fit in [src], make some space!</span>"
		return 0

	if(W.w_class >= w_class && (istype(W, /obj/item/weapon/storage)))
		if(!istype(src, /obj/item/weapon/storage/backpack/holding))
			if(!stop_messages)
				usr << "<span class='warning'>[src] cannot hold [W] as it's a storage item of the same size!</span>"
			return 0

	if(W.flags & NODROP)
		usr << "<span class='warning'>\the [W] is stuck to your hand, you can't put it in \the [src]!</span>"
		return 0

	return 1


/obj/item/clothing/suit/storage/proc/handle_item_insertion(obj/item/W, prevent_warning = 0, mob/user)
	if(!istype(W)) return 0
	if(usr)
		if(!usr.unEquip(W))
			return 0
	if(silent)
		prevent_warning = 1
	W.loc = src
	W.on_enter_storage(src)
	if(usr)
		if(usr.client && usr.s_active != src)
			usr.client.screen -= W

		add_fingerprint(usr)

		if(!prevent_warning && !istype(W, /obj/item/weapon/gun/energy/kinetic_accelerator/crossbow))
			for(var/mob/M in viewers(usr, null))
				if(M == usr)
					usr << "<span class='notice'>You put [W] [preposition]to [src].</span>"
				else if(in_range(M, usr))
					M.show_message("<span class='notice'>[usr] puts [W] [preposition]to [src].</span>", 1)
				else if(W && W.w_class >= 3)
					M.show_message("<span class='notice'>[usr] puts [W] [preposition]to [src].</span>", 1)

		orient2hud(usr)
		for(var/mob/M in can_see_contents())
			show_to(M)
	W.mouse_opacity = 2
	update_icon()
	return 1


/obj/item/clothing/suit/storage/proc/remove_from_storage(obj/item/W, atom/new_location)
	if(!istype(W)) return 0

	if(istype(src, /obj/item/weapon/storage/fancy))
		var/obj/item/weapon/storage/fancy/F = src
		F.update_icon(1)

	for(var/mob/M in can_see_contents())
		if(M.client)
			M.client.screen -= W

	if(ismob(loc))
		var/mob/M = loc
		W.dropped(M)
	W.layer = initial(W.layer)
	W.loc = new_location

	if(usr)
		orient2hud(usr)
		if(usr.s_active)
			usr.s_active.show_to(usr)
	if(W.maptext)
		W.maptext = ""
	W.on_exit_storage(src)
	update_icon()
	W.mouse_opacity = initial(W.mouse_opacity)
	return 1


/obj/item/clothing/suit/storage/attackby(obj/item/W, mob/user, params)
	..()

	if(isrobot(user))
		user << "<span class='warning'>You're a robot. No.</span>"
		return 0

	if(!can_be_inserted(W, 0 , user))
		return 0

	handle_item_insertion(W, 0 , user)
	return 1


/obj/item/clothing/suit/storage/dropped(mob/user)
	return

/obj/item/clothing/suit/storage/verb/Open_Das_Stuf(mob/user)
	set name = "Open pockets"
	playsound(loc, "rustle", 50, 1, -5)

	if(ishuman(user))
		var/mob/living/carbon/human/H = user
		if(H.l_store == src && !H.get_active_hand())
			H.put_in_hands(src)
			H.l_store = null
			return
		if(H.r_store == src && !H.get_active_hand())
			H.put_in_hands(src)
			H.r_store = null
			return

	orient2hud(user)
	if(loc == user)
		if(user.s_active)
			user.s_active.close(user)
		show_to(user)
	else
		..()
		for(var/mob/M in range(1))
			if(M.s_active == src)
				close(M)
	add_fingerprint(user)

/obj/item/clothing/suit/storage/attack_paw(mob/user)
	return Open_Das_Stuf(user)


/obj/item/clothing/suit/storage/New()
	..()

	boxes = new /obj/screen/storage()
	boxes.name = "storage"
	boxes.master = src
	boxes.icon_state = "block"
	boxes.screen_loc = "7,7 to 10,8"
	boxes.layer = 19
	orient2hud()

/obj/item/clothing/suit/storage/Destroy()
	for(var/obj/O in contents)
		O.mouse_opacity = initial(O.mouse_opacity)

	close_all()
	qdel(boxes)
	qdel(closer)
	return ..()
