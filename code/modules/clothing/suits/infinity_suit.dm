/obj/item/clothing/suit/antiquated_cape
	name = "antiquated cape"
	desc = "A some old and... familiar cape?"
	icon_state = "antiquated_cape"
	item_state = "ro_suit"
	item_color = "antiquated_cape"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/hooded/black_hoody
	name = "black hoody"
	desc = "Warm black hoody."
	icon_state = "black_hoody"
	item_state = "bl_suit"
	item_color = "black_hoody"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	hooded = 1
	action_button_name = "Toggle Hood"
	hoodtype = /obj/item/clothing/head/blackhoody

/obj/item/clothing/head/blackhoody
	icon_state = "blackhoody"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	flags = NODROP
	body_parts_covered = HEAD

/obj/item/clothing/suit/brand/blue_jacket
	name = "brand jacket"
	desc = "Brand jacket, maked by outerspace designer."
	icon_state = "brand_jacket"
	item_state = "b_suit"
	item_color = "brand_jacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/brand/orange_jacket
	name = "brand orange jacket"
	desc = "Brand jacket, maked by outerspace designer."
	icon_state = "brand_orange_jacket"
	item_state = "o_suit"
	item_color = "brand_orange_jacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/toggle/fiery_jacket
	name = "fiery jacket"
	desc = "A cool jacket, with fire logo on the back."
	icon_state = "fiery_jacket"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	togglename = "zipper"

/obj/item/clothing/suit/toggle/white_fiery_jacket
	name = "white fiery jacket"
	desc = "A cool white jacket, with blue fire logo on the back."
	icon_state = "white_fiery_jacket"
	item_state = "w_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	togglename = "zipper"

/obj/item/clothing/suit/leon_jacket
	name = "leon jacket"
	desc = "Light, leather jacket."
	icon_state = "leon_jacket"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
