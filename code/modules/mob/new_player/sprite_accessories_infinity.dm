/datum/sprite_accessory/underwear/stripper_p
	name = "Stripper Pink"
	icon_state = "stripper_p"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/stripper_g
	name = "Stripper Green"
	icon_state = "stripper_g"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/swim_blue
	name = "Blue swimsuit"
	icon_state = "swim_blue"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/swim_green
	name = "Green Swimsuit"
	icon_state = "swim_green"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/swim_red
	name = "Red Swimsuit"
	icon_state = "swim_red"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/swim_black
	name = "Black Swimsuit"
	icon_state = "swim_black"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/swim_purp
	name = "Purple Swimsuit"
	icon_state = "swim_purp"
	icon = 'icons/mob/paradise_clothes.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bluestrippedunderwear
	name = "Blue Stripped Underwear"
	icon_state = "bluestrippedunderwear"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/redstrippedunderwear
	name = "Red Stripped Underwear"
	icon_state = "redstrippedunderwear"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/blackstrippedunderwear
	name = "Black Stripped Underwear"
	icon_state = "blackstrippedunderwear"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/hair/dads
	name = "Dads"
	icon_state = "dads"
	icon = 'icons/mob/more_hairs.dmi'

/datum/sprite_accessory/socks/blue_thigh
	name = "Thigh-high Blue"
	icon_state = "blue_thigh"
	icon = 'icons/mob/infinity_inriss.dmi'
	gender = FEMALE

/datum/sprite_accessory/socks/black_thigh
	name = "Thigh-high Black"
	icon_state = "black_thigh"
	icon = 'icons/mob/infinity_inriss.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikiniredblack
	name = "Red-Black Bikini"
	icon_state = "bikini_red_black"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikiniblueblack
	name = "Blue-Black Bikini"
	icon_state = "bikini_blue_black"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikiniyellowblack
	name = "Yellow-Black Bikini"
	icon_state = "bikini_yellow_black"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikinigreenblack
	name = "Green-Black Bikini"
	icon_state = "bikini_green_black"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikiniredwhite
	name = "Red-White Bikini"
	icon_state = "bikini_red_white"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/underwear/bikininain
	name = "Nain Bikini"
	icon_state = "bikini_nain"
	icon = 'icons/mob/infinity_work.dmi'
	gender = FEMALE

/datum/sprite_accessory/hair/pigtailis
	name = "Pigtailis"
	icon_state = "pigtailis"
	icon = 'icons/mob/more_hairs.dmi'